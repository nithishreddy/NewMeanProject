import { Component } from '@angular/core';
import{AppRoutes} from './app.routes';

@Component({
    moduleId: module.id,
    selector: 'mean-app',
    templateUrl: 'app.template.html',
    styleUrls:['app.component.css'],
})
export class AppComponent {
   
 }